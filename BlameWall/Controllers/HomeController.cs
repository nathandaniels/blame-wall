﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BlameWall.Models.Configuration;
using System.Threading.Tasks;
using BlameWall.Models.Jenkins;

namespace BlameWall.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: Home
        [HttpGet]
        [NoAsyncTimeout]
        public async Task<ActionResult> Configure(Boolean reload = false)
        {
            var configuration = Loader.Load();

            if (reload)
            {
                configuration.Jobs = new JenkinsClient(configuration.JenkinsUrl).GetJobs().Select(j =>
                {
                    var existing = configuration.Jobs?.FirstOrDefault(j2 => j2.RealName.Equals(j.Name));

                    return new Job()
                    {
                        Displayed = existing == null ? false : existing.Displayed,
                        RealName = j.Name,
                        DisplayName = j.DisplayName,
                        Culprit = existing?.Culprit,
                        CulpritSpecifiedFor = existing == null ? 0 : existing.CulpritSpecifiedFor,
                        Url = j.RestUrl
                    };
                }).ToArray();

                Loader.Save(configuration);
            }

            return View(configuration);
        }

        [HttpPost]
        public ActionResult Configure(Configuration model)
        {
            Loader.Save(model);

            return View(model);
        }
    }
}