﻿using BlameWall.Models;
using BlameWall.Models.Configuration;
using BlameWall.Models.Jenkins;
using Jenkins.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BlameWall.Controllers
{
    public class BlameController : Controller
    {
        // GET: Blame
        public ActionResult Index()
        {
            var config = Loader.Load();
            var client = new JenkinsClient(config.JenkinsUrl);

            var viewModel = new BlameViewModel()
            {
                Blames = config.Jobs
                    .Where(j=>j.Displayed)
                    .Select(j => new { Job = client.GetJob(j.Url), Culprit = j.Culprit })
                    .Where(b => b.Job.Status.State == JenkinsJobState.Failed || b.Job.Status.State == JenkinsJobState.Unstable)
                    .Select(j => new Blame()
                    {
                        CulpritName = j.Culprit,
                        BuildName = j.Job.Name,
                        PossibleCulprits = (client.GetBuild(j.Job.LastFailedBuild?.RestUrl).Culprits?
                            .Select(c => c.FullName.Split(' ').First())
                            .ToList()) ?? new List<String>()
                    }).ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(BlameViewModel model)
        {
            var config = Loader.Load();
            var client = new JenkinsClient(config.JenkinsUrl);
            var jobs = client.GetJobs();

            foreach (var blamed in model.Blames)
            {
                var configEntry = config.Jobs.FirstOrDefault(j => j.RealName.Equals(blamed.BuildName));
                var serverJob = jobs.FirstOrDefault(j => j.Name.Equals(blamed.BuildName));

                if (configEntry != null && serverJob != null)
                {
                    configEntry.Culprit = blamed.CulpritName;
                    configEntry.CulpritSpecifiedFor = client.GetJob(serverJob.RestUrl).LastBuild.Number;
                }
            }

            Loader.Save(config);

            return this.RedirectToAction("Index", "Home");
        }
    }
}