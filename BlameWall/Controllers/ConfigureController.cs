﻿using System;
using System.Linq;
using System.Web.Mvc;

using BlameWall.Models.Configuration;
using BlameWall.Models.Jenkins;

namespace BlameWall.Controllers
{
    public class ConfigureController : Controller
    {
        [HttpGet]
        public ActionResult Index(Boolean reload = false)
        {
            var configuration = Loader.Load();

            if (reload)
            {
                configuration.Jobs = new JenkinsClient(configuration.JenkinsUrl).GetJobs().Select(j =>
                {
                    var existing = configuration.Jobs?.FirstOrDefault(j2 => j2.RealName.Equals(j.Name));

                    return new Job()
                    {
                        Displayed = existing == null ? false : existing.Displayed,
                        RealName = j.Name,
                        DisplayName = existing?.DisplayName ?? j.DisplayName,
                        Culprit = existing?.Culprit,
                        CulpritSpecifiedFor = existing == null ? 0 : existing.CulpritSpecifiedFor,
                        Url = j.RestUrl,
                        Index = existing?.Index ?? 0
                    };
                }).ToArray();

                Loader.Save(configuration);
            }

            return View(configuration);
        }

        [HttpPost]
        public ActionResult Index(Configuration model)
        {
            var oldJenkins = Loader.Load().JenkinsUrl;

            if (!model.JenkinsUrl.StartsWith("http"))
            {
                model.JenkinsUrl = $"http://{model.JenkinsUrl}";
            }

            Loader.Save(model);

            if (oldJenkins?.Equals(model.JenkinsUrl) ?? false)
            {
                return RedirectToAction("Index", "Home");
            }

            return this.Index(true);
        }
    }
}