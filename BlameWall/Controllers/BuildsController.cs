﻿using BlameWall.Models;
using BlameWall.Models.Configuration;
using BlameWall.Models.Jenkins;
using Jenkins.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlameWall.Controllers
{
    public class BuildsController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<BuildStatus> Get()
        {
            var config = Loader.Load();

            var tasks = new LinkedList<Task<BuildStatus>>();

            foreach (var job in config.Jobs.Where(j => j.Displayed).OrderBy(j=>j.Index))
            {
                tasks.AddLast(Task.Run(() => this.GetStatus(config, job)));
            }

            foreach (var t in tasks)
            {
                if (t.Result != null)
                {
                    yield return t.Result;
                }
            }
        }

        private BuildStatus GetStatus(Configuration config, Job job)
        {
            var client = new JenkinsClient(config.JenkinsUrl);
            var jenkinsBuild = client.GetJob(job.Url);

            var lastRun = jenkinsBuild.LastBuild == null ? null : client.GetBuild(jenkinsBuild.LastBuild.RestUrl);

            var buildStatus = new BuildStatus();

            buildStatus.Name = job.DisplayName;
            buildStatus.IsRebuilding = lastRun?.IsBuilding ?? false;

            switch (jenkinsBuild.Status.State)
            {
                case JenkinsJobState.Success:
                    buildStatus.BuildState = BuildStatus.State.Success;

                    buildStatus.StatusFor = GetTimeSince(GetNextBuildTime(jenkinsBuild, jenkinsBuild.LastFailedBuild, client));

                    break;
                case JenkinsJobState.Failed:
                case JenkinsJobState.Unstable:
                    buildStatus.BuildState = BuildStatus.State.Fail;

                    buildStatus.StatusFor = GetTimeSince(GetNextBuildTime(jenkinsBuild, jenkinsBuild.LastSuccessfulBuild, client));

                    if (lastRun != null)
                    {
                        var lastFailed = client.GetBuild(jenkinsBuild.LastFailedBuild?.RestUrl);

                        if (lastFailed != null && lastFailed.Culprits != null)
                        {
                            buildStatus.Culprit = (String.IsNullOrWhiteSpace(job.Culprit) || job.CulpritSpecifiedFor != lastFailed.Number)?
                                lastFailed.Culprits.Count() > 4 ? "" :
                                String.Join(", ", lastFailed.Culprits.Select(c => c.FullName.Split(' ').FirstOrDefault()))
                            : job.Culprit;
                        }
                    }

                    break;
                case JenkinsJobState.None:
                case JenkinsJobState.Aborted:
                    buildStatus.BuildState = BuildStatus.State.Unknown;

                    var last = (jenkinsBuild.LastFailedBuild?.Number ?? 0) > (jenkinsBuild.LastSuccessfulBuild?.Number ?? 0) ? jenkinsBuild.LastBuild : jenkinsBuild.LastSuccessfulBuild;
                    last = last?.Number == lastRun?.Number ? jenkinsBuild.Builds.OrderBy(b => b.Number).LastOrDefault(b => b.Number < last?.Number) : last;
                    buildStatus.StatusFor = GetTimeSince(GetNextBuildTime(jenkinsBuild, last, client));

                    break;
                case JenkinsJobState.Disabled:
                    return null;
            }

            return buildStatus;
        }

        /// <summary>
        /// Gets the date/time the next build after the specified build ended.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="build"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        private static DateTime? GetNextBuildTime(JenkinsJob job, JenkinsBuild build, JenkinsClient client)
        {
            if (build != null)
            {
                var number = build.Number + 1;
                var lastUrl = job.Builds.OrderBy(b => b.Number).FirstOrDefault(b => b.Number >= number)?.RestUrl;
                var nextBuild = client.GetBuild(lastUrl);
                if (nextBuild != null)
                {
                    return nextBuild.TimeStamp + (nextBuild.Duration ?? nextBuild.EstimatedDuration ?? new TimeSpan(0));
                }
            }

            return null;
        }

        private static String GetTimeSince(DateTime? date)
        {
            if (date == null)
            {
                return "Forver";
            }

            var diff = DateTime.Now - date.Value;

            var mins = diff.TotalMinutes;
            var hours = diff.TotalHours;
            var days = diff.TotalDays;

            if (days > 2)
            {
                return $"{Math.Round(days, 0)} days";
            }
            else if (hours > 2)
            {
                return $"{Math.Round(hours, 0)} hours";
            }
            else
            {
                return $"{Math.Round(mins, 0)} minutes";
            }
        }
    }
}