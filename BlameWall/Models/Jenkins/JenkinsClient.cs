﻿using Jenkins.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace BlameWall.Models.Jenkins
{
    public class JenkinsClient
    {
        public Uri JenkinsUrl { get; }

        public JenkinsClient(String url)
        {
            this.JenkinsUrl = new Uri(url);
        }

        public IEnumerable<JenkinsJob> GetJobs()
        {
            var jobsUri = new Uri(this.JenkinsUrl, "/api/json");
            var server = JObject.Parse(new WebClient().DownloadString(jobsUri));
            var jobs = JsonConvert.DeserializeObject<IEnumerable<JenkinsJob>>(server["jobs"].ToString());

            return jobs;
        }

        internal JenkinsJob GetJob(string url)
        {
            if (url == null)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<JenkinsJob>(new WebClient().DownloadString(url));
        }

        internal JenkinsBuild GetBuild(string url)
        {
            if (url == null)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<JenkinsBuild>(new WebClient().DownloadString(url));
        }
    }
}