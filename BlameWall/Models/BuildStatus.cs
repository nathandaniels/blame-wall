﻿using System;

namespace BlameWall.Models
{
    public class BuildStatus
    {
        public String Name { get; set; }

        public State BuildState { get; set; }

        public String Culprit { get; set; }

        public Boolean IsRebuilding { get; set; }

        public String StatusFor { get; set; }

        public enum State
        {
            Success,
            Fail,
            Unknown
        }
    }
}