﻿using System;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Web.Hosting;

namespace BlameWall.Models.Configuration
{
    public static class Loader
    {
        private static Object lockObject = new object();

        private static String FileName;

        private static Configuration cached;

        static Loader()
        {
            FileName = HostingEnvironment.MapPath("~/App_Data/config.json");
        }

        public static Configuration Load()
        {
            if (cached != null)
            {
                return cached;
            }

            var serializer = new DataContractJsonSerializer(typeof(Configuration));

            lock (lockObject)
            {
                if (!File.Exists(FileName))
                {
                    return new Configuration() { Jobs = new Job[0] };
                }

                using (var stream = File.OpenRead(FileName))
                {
                    var config = serializer.ReadObject(stream) as Configuration;
                    return config;
                }
            }
        }

        public static void Save(Configuration config)
        {
            cached = config;

            var serializer = new DataContractJsonSerializer(typeof(Configuration));

            lock (lockObject)
            {
                File.Delete(FileName);
                using (var stream = File.OpenWrite(FileName))
                {
                    serializer.WriteObject(stream, config);
                }
            }
        }
    }
}