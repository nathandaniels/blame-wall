﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BlameWall.Models.Configuration
{
    [DataContract]
    public class Configuration
    {
        [DataMember]
        public Job[] Jobs { get; set; }

        [DataMember]
        public String JenkinsUrl { get; set; }
    }
}