﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BlameWall.Models.Configuration
{
    [DataContract]
    public class Job
    {
        [DataMember]
        public Boolean Displayed { get; set; }

        [DataMember]
        public String DisplayName { get; set; }

        [DataMember]
        public String RealName { get; set; }

        [DataMember]
        public String Culprit { get; set; }

        [DataMember]
        public Int32 CulpritSpecifiedFor { get; set; }

        [DataMember]
        public String Url { get; set; }

        [DataMember]
        public Int32 Index { get; set; }
    }
}