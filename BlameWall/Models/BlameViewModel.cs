﻿using System;
using System.Collections.Generic;
namespace BlameWall.Models
{
    public class BlameViewModel
    {
        public ICollection<Blame> Blames { get; set; } = new List<Blame>();
    }

    public class Blame
    {
        public ICollection<String> PossibleCulprits { get; set; }

        public String CulpritName { get; set; }

        public String BuildName { get; set; }
    }
}